from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve, reverse
from selenium import webdriver
from . import views
import time

# Create your tests here.

class Story7LiveTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_index(self):
        self.browser.get(self.live_server_url + '/resume/')

        self.assertInHTML('My Resume', self.browser.page_source)
        self.browser.find_element_by_id("ui-id-1").click()
        
        button = self.browser.find_element_by_css_selector('.slider')
        button.click()
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, "rgba(0, 0, 0, 1)")
        self.tearDown()

class Story7Test(TestCase):
    def test_story_7_url_is_exist(self):
        self.response = Client().get('/resume/')
        self.assertEqual(self.response.status_code, 200)

    def test_story_6_url_is_not_exist(self):
        self.response = Client().get('/error')
        self.assertEqual(self.response.status_code, 404)
        
    def test_status_show_content(self):
        self.response = Client().get('/resume/')
        self.assertIn('</h1>', self.response.content.decode())
        self.assertIn('</h3>', self.response.content.decode())
        self.assertIn('</p>', self.response.content.decode())
