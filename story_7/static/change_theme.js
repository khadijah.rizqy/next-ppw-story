(function() {
    if ($('#theme').is(':checked')) {
      $('html').attr('theme', 'dark')
    }
  
    $('#theme').click(function() {
      if ($(this).is(':checked')) {
        $('html').attr('theme', 'dark')
      } else {
        $('html').removeAttr('theme')
      }
    });
  })();
  