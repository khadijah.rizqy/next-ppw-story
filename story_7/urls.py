from django.urls import path
from .views import resume

app_name = "story_7"

urlpatterns = [
    path('resume/', resume, name="resume"),
]
