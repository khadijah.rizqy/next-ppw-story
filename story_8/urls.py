from django.urls import path
from .views import books_list

app_name = "story_8"

urlpatterns = [
    path('story_8/', books_list, name="books_list"),
]