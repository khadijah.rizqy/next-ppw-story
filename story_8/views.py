import requests
import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

# Create your views here.

def books_list(request):
    return render(request, 'books_list.html')

