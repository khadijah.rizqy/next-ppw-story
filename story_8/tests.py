from django.test import TestCase, Client
from django.urls import resolve
from .views import books_list

# Create your tests here.

class Story8Test(TestCase):
    # testing whether the url exist or not
    def test_landing_url_is_exist(self):
        response = Client().get('/story_8/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_url_is_not_exist(self):
        self.response = Client().get('/error')
        self.assertEqual(self.response.status_code, 404)
        
    def test_status_show_content(self):
        self.response = Client().get('/story_8/')
        self.assertIn('</h1>', self.response.content.decode())
        self.assertIn('</form>', self.response.content.decode())
        self.assertIn('</button>', self.response.content.decode())
        self.assertIn('</table>', self.response.content.decode())
