from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import register

# Create your tests here.
class Story9Test(TestCase):

    def test_story_9_url_is_exist(self):
        response = Client().get('/story_9/')
        self.assertEqual(response.status_code, 200)

    def test_error_url_is_not_exist(self):
        response = Client().get('/error')
        self.assertEqual(response.status_code, 404)

    def test_login_using_right_template(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_logout_using_right_template(self):
        response = Client().get('/accounts/logout/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/logged_out.html')

    def test_status_show_content(self):
        self.response = Client().get(reverse('story_9:register'))
        self.assertIn('</title>', self.response.content.decode('utf-8'))
        self.assertIn('button', self.response.content.decode('utf-8'))
        