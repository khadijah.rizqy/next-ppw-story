from django.urls import path, include
from story_9 import views
from .views import register
from django.shortcuts import render

app_name = 'story_9'

urlpatterns = [
    path('story_9/', register, name='register'),
]
