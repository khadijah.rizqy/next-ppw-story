from django.urls import path
from .views import add_status

app_name = "story_6"

urlpatterns = [
    path('', add_status, name="add_status"),
]