from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status_text',]
        labels = {'status_text' : 'Status'}
        widgets = {
            'status_text' : forms.TextInput(attrs={'class' : 'form-control',
                'type': 'text', 'placeholder' : 'Please write your status here.'})
        } 
