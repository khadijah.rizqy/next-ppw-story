from django.test import LiveServerTestCase, TestCase, Client
from django.utils import timezone
from django.urls import resolve
from selenium import webdriver
import time
from .models import Status

# Create your tests here.

class Story6LiveTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_index(self):
        self.browser.get(self.live_server_url)
        
        time.sleep(5)

        self.assertInHTML('Halo, apa kabar?', self.browser.page_source)

        status = self.browser.find_element_by_class_name('form-control')
        button = self.browser.find_element_by_class_name('button')

        status.send_keys('good, how are you?')
        time.sleep(3)
        button.click()

        self.browser.get(self.live_server_url)
        self.assertInHTML('good, how are you?', self.browser.page_source)
        self.tearDown()

class Story6Test(TestCase):
    # testing url for story_6
    def test_story_6_url_is_exist(self):
        self.response = Client().get('')
        self.assertEqual(self.response.status_code, 200)

    def test_story_6_url_is_not_exist(self):
        self.response = Client().get('/error')
        self.assertEqual(self.response.status_code, 404)
        
    def test_status_show_content(self):
        self.response = Client().get('')
        self.assertIn('</form>', self.response.content.decode())
        self.assertIn('</h1>', self.response.content.decode())
        self.assertIn('type = "submit"', self.response.content.decode())

    def test_status_show_result(self):
        self.response = self.client.post('', follow=True, data= {
            'status_text' : 'good, and how are you?',
        })

        self.assertContains(self.response, 'good, and how are you?', html=True)
